import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:provider/provider.dart';

import '../providers/cart.dart';
import '../providers/orders.dart';
import '../widgets/cart_item.dart';

class CartScreen extends StatefulWidget {
  static String routeName = '/cart';

  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  bool _isLoading = false;
  Future addOrder(Orders orders, Cart cart, BuildContext context) async {
    try {
      setState(() {
        _isLoading = true;
      });
      await orders.addOrder(cart.items.values.toList(), cart.totalExpense());
      cart.clear();
    } catch (error) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(error.toString())));
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final Cart cart = Provider.of<Cart>(context, listen: true);
    final Orders orders = Provider.of<Orders>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Your Cart"),
      ),
      body: cart.items.isEmpty
          ? Center(
              child: Text("Your cart is empty!"),
            )
          : Container(
              padding:
                  EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 10),
              width: double.infinity,
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Total Items :",
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        "  ${cart.allProductsCount}",
                        style: TextStyle(
                            fontSize: 20,
                            color: Theme.of(context).colorScheme.secondary,
                            fontWeight: FontWeight.bold),
                      ),
                      Spacer(),
                      Chip(
                        padding: EdgeInsets.all(10),
                        label: Text(
                          "\$ ${cart.totalExpense().toStringAsFixed(2)}",
                          style: TextStyle(color: Colors.white),
                        ),
                        backgroundColor: Theme.of(context).colorScheme.primary,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: cart.items.values
                          .map((e) => CartItemWidget(
                                cartItem: e,
                              ))
                          .toList(),
                    ),
                  ),
                  Spacer(),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor:
                            Theme.of(context).colorScheme.secondary),
                    onPressed: _isLoading
                        ? null
                        : () {
                            addOrder(orders, cart, context);
                          },
                    child: Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(20),
                        width: double.infinity,
                        child: _isLoading
                            ? CircularProgressIndicator(
                                color: Theme.of(context).colorScheme.onPrimary,
                              )
                            : Text(
                                "Order Now!",
                                style: TextStyle(fontSize: 16),
                              )),
                  )
                ],
              ),
            ),
    );
  }
}
