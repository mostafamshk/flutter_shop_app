import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/widgets/drawer.dart';
import 'package:intl/intl.dart';

import '../providers/orders.dart';
import '../widgets/order_item.dart';

class OrdersScreen extends StatefulWidget {
  static String routeName = '/orders';

  const OrdersScreen({super.key});

  @override
  State<OrdersScreen> createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  bool _isLoading = false;
  bool _isInit = true;
  String message = "";

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Orders>(context).getOrders().then((_) {
        setState(() {
          _isLoading = false;
        });
      }).catchError((error) {
        setState(() {
          _isLoading = false;
          message = "Could not load your orders!";
        });
      });
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final ordersProvider = Provider.of<Orders>(context);
    return Scaffold(
        drawer: DrawerWidget(),
        appBar: AppBar(
          title: Text("Orders"),
        ),
        body: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : message.isNotEmpty
                ? Center(
                    child: Text(message),
                  )
                : ordersProvider.orders.isEmpty
                    ? Center(
                        child: Text("You have no orders submitted!"),
                      )
                    : Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(10),
                        child: ListView.builder(
                          itemCount: ordersProvider.orders.length,
                          itemBuilder: (context, index) => OrderItemWidget(
                              orderId: ordersProvider.orders[index].id),
                        )));
  }
}
