// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/screens/cart_screen.dart';

import '../providers/cart.dart';
import '../providers/product.dart';
import '../providers/products.dart';

class ProductDetailScreen extends StatelessWidget {
  static const routeName = '/product-detail';

  const ProductDetailScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String productId =
        ModalRoute.of(context)!.settings.arguments as String;
    Product product = Provider.of<Products>(context, listen: false)
        .items
        .firstWhere((product) => product.id == productId);
    return Scaffold(
      appBar: AppBar(
        title: Text(product.title),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 300,
              width: double.infinity,
              child: Card(
                elevation: 5,
                child: Hero(
                  tag: product.id,
                  child: Image.network(
                    product.imageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    product.title,
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "\$ ${product.price}",
                    style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).colorScheme.primary),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Text(
                product.description,
                textAlign: TextAlign.start,
              ),
            ),
            Spacer(),
            Consumer<Cart>(
              builder: (BuildContext context, cart, Widget? child) {
                return Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  elevation: 10,
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                            color: Theme.of(context).colorScheme.primary,
                            width: 2)),
                    padding: EdgeInsets.all(10),
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        cart.isProductInCart(productId)
                            ? Row(
                                children: [
                                  IconButton(
                                    iconSize: 30,
                                    color:
                                        Theme.of(context).colorScheme.secondary,
                                    onPressed: () {
                                      cart.removeOneProductFromCart(productId);
                                    },
                                    icon: Icon(Icons.remove),
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: Theme.of(context)
                                            .colorScheme
                                            .secondary),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    cart.items[productId]!.quantity.toString(),
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  IconButton(
                                    iconSize: 30,
                                    color:
                                        Theme.of(context).colorScheme.secondary,
                                    onPressed: () {
                                      cart.addToCart(productId, product.price,
                                          product.title);
                                    },
                                    icon: Icon(Icons.add),
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: Theme.of(context)
                                            .colorScheme
                                            .secondary),
                                  ),
                                ],
                              )
                            : ElevatedButton(
                                onPressed: () {
                                  cart.addToCart(
                                      productId, product.price, product.title);
                                },
                                child: Text("Add To Cart"),
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: Theme.of(context)
                                        .colorScheme
                                        .secondary),
                              ),
                        Spacer(),
                        Text(
                          "\$ ${cart.isProductInCart(productId) ? (product.price * cart.items[productId]!.quantity).toStringAsFixed(2) : product.price.toStringAsFixed(2)}",
                          style: TextStyle(
                              fontSize: 20,
                              color: Theme.of(context).colorScheme.primary),
                        ),
                        if (cart.isProductInCart(productId))
                          IconButton(
                              iconSize: 30,
                              color: Theme.of(context).colorScheme.secondary,
                              onPressed: () {
                                Navigator.of(context)
                                    .pushNamed(CartScreen.routeName);
                              },
                              icon: Icon(Icons.shopping_cart))
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
