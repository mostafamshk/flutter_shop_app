import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/screens/cart_screen.dart';
import 'package:shopapp/widgets/drawer.dart';

import 'package:shopapp/widgets/products_grid.dart';
import '../providers/cart.dart';
import '../providers/products.dart';
import '../widgets/badge.dart';

enum FilterOptions { Favorites, All }

class ProductsOverviewScreen extends StatefulWidget {
  static const routeName = '/products-overview';

  const ProductsOverviewScreen({super.key});

  @override
  State<ProductsOverviewScreen> createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  FilterOptions selectedFilter = FilterOptions.All;
  bool _isInit = true;
  bool _isLoading = false;
  String pageErrorMessage = '';

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        pageErrorMessage = "";
        _isLoading = true;
      });

      Provider.of<Products>(context).getProducts(false).then((_) {
        setState(() {
          pageErrorMessage = "";
          _isLoading = false;
        });
      }).catchError((error) {
        setState(() {
          pageErrorMessage = "Something went wrong. Failed to load products!";
          _isLoading = false;
        });
      });
      _isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // Cart cartProvider = Provider.of<Cart>(context, listen: false);

    return Scaffold(
      drawer: DrawerWidget(),
      appBar: AppBar(
        title: Text("Shop"),
        actions: [
          Consumer<Cart>(
            builder: (ctx, cart, ch) => BadgeWidget(
              value: cart.allProductsCount.toString(),
              color: Theme.of(context).colorScheme.secondary,
              child: ch as Widget,
            ),
            child: IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.of(context).pushNamed(CartScreen.routeName);
              },
            ),
          ),
          PopupMenuButton(
            icon: Icon(Icons.more_vert),
            onSelected: (value) {},
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text("Show Favorites"),
                  onTap: () {
                    setState(() {
                      selectedFilter = FilterOptions.Favorites;
                    });
                  },
                ),
                PopupMenuItem(
                  child: Text("Show All"),
                  onTap: () {
                    setState(() {
                      selectedFilter = FilterOptions.All;
                    });
                  },
                ),
              ];
            },
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        width: double.infinity,
        child: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : pageErrorMessage.isNotEmpty
                ? Center(
                    child: Text(
                      pageErrorMessage,
                      style:
                          TextStyle(color: Theme.of(context).colorScheme.error),
                    ),
                  )
                : selectedFilter == FilterOptions.Favorites &&
                        !Provider.of<Products>(context).hasFavorites()
                    ? Center(
                        child: Text("No favorite product added!"),
                      )
                    : ProductsGrid(
                        showFavorites:
                            selectedFilter == FilterOptions.Favorites),
      ),
    );
  }
}
