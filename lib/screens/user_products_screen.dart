import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/screens/edit_user_product_screen.dart';
import 'package:shopapp/widgets/user_product_item.dart';
import '../providers/products.dart';
import '../widgets/drawer.dart';

class UserProductsScreen extends StatefulWidget {
  static const routeName = '/user-products-screen';

  const UserProductsScreen({super.key});

  @override
  State<UserProductsScreen> createState() => _UserProductsScreenState();
}

class _UserProductsScreenState extends State<UserProductsScreen> {
  bool _isLoading = false;
  String? message = null;

  Future<void> onPageRefresh(BuildContext context) async {
    setState(() {
      _isLoading = true;
    });
    try {
      await Provider.of<Products>(context, listen: false).getProducts(true);
    } catch (exception) {
      setState(() {
        message = "Could not load your products";
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    Future.delayed(Duration.zero).then((value) {
      onPageRefresh(context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final products = Provider.of<Products>(context, listen: true);
    return Scaffold(
      drawer: const DrawerWidget(),
      appBar: AppBar(
        title: Text("Your products"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context).pushNamed(EditUserProductScreen.routeName,
                    arguments: "null");
              },
              icon: Icon(Icons.add))
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () => onPageRefresh(context),
        child: Container(
            width: double.infinity,
            padding: const EdgeInsets.all(20),
            child: _isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    // padding: EdgeInsets.symmetric(vertical: 30),
                    itemBuilder: (context, index) => UserProductItem(
                        productId: products.items[index].id,
                        title: products.items[index].title,
                        imageUrl: products.items[index].imageUrl),
                    itemCount: products.items.length,
                  )),
      ),
    );
  }
}
