import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/providers/product.dart';

import '../providers/auth.dart';
import '../providers/products.dart';

class EditUserProductScreen extends StatefulWidget {
  static const routeName = '/edit-user-product-screen';



  const EditUserProductScreen({super.key});

  @override
  State<EditUserProductScreen> createState() => _EditUserProductScreenState();
}

class _EditUserProductScreenState extends State<EditUserProductScreen> {
  final _formKey = GlobalKey<FormState>();
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  // final userId = Provider.of<Auth>(context, listen: false);
  bool showImage = false;
  bool isAddingProduct = true;
  Product edittedProduct = Product(
      id: DateTime.now().toString(),
      title: "",
      description: "",
      price: -1,
      imageUrl: "",
      isFavorite: false);
  Map<String, String> formInitialValues = {
    'title': "",
    'price': '',
    'description': '',
    'imageUrl': ''
  };
  bool _isLoading = false;
  void loadImage() {
    if (!_imageUrlFocusNode.hasFocus) {
      setState(() {
        showImage = true;
      });
    }
  }

  Future<void> onFormSave() async {
    bool succeeded = true;
    final isValid = _formKey.currentState!.validate();
    if (!isValid) return;
    _formKey.currentState!.save();
    setState(() {
      _isLoading = true;
    });

    try {
      final response = isAddingProduct
          ? await Provider.of<Products>(context, listen: false).addProduct(
              Product(
                  id: edittedProduct.id,
                  title: edittedProduct.title,
                  description: edittedProduct.description,
                  price: edittedProduct.price,
                  imageUrl: edittedProduct.imageUrl,
                  isFavorite: edittedProduct.isFavorite))
          : await Provider.of<Products>(context, listen: false)
              .updateProduct(edittedProduct.id, edittedProduct);
    } catch (error) {
      succeeded = false;
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text("An error occured!"),
          content: Text("Process failed due to some issues."),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(ctx, rootNavigator: true).pop(true);
                  setState(() {
                    _isLoading = false;
                  });
                  Navigator.of(context).pop();
                },
                child: Text("Okay"))
          ],
        ),
      );
    } finally {
      setState(() {
        _isLoading = false;
      });
      if (succeeded) Navigator.of(context).pop();
    }
  }

  @override
  void initState() {
    _imageUrlFocusNode.addListener(loadImage);
    super.initState();
  }

  @override
  void dispose() {
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlController.dispose();
    _imageUrlFocusNode.removeListener(loadImage);
    super.dispose();
  }

  bool isInit = true;
  @override
  void didChangeDependencies() {
    if (isInit) {
      final productId = ModalRoute.of(context)!.settings.arguments as String;
      if (productId != "null") {
        showImage = true;
        isAddingProduct = false;
        final product = Provider.of<Products>(context).findById(productId);
        edittedProduct = Product(
            id: product.id,
            title: product.title,
            description: product.description,
            price: product.price,
            imageUrl: product.imageUrl,
            isFavorite: product.isFavorite);
        formInitialValues = {
          'title': product.title,
          'price': product.price.toString(),
          'description': product.description,
          'imageUrl': product.imageUrl
        };
        _imageUrlController.text = product.imageUrl;
      }
    }
    isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${isAddingProduct ? "Add" : "Edit"} product"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          onFormSave();
        },
        child: Icon(isAddingProduct ? Icons.add : Icons.edit),
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(30),
        child: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Form(
                key: _formKey,
                child: ListView(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      initialValue: formInitialValues['title'],
                      decoration: InputDecoration(labelText: "Title"),
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.text,
                      onFieldSubmitted: (value) {
                        FocusScope.of(context).requestFocus(_priceFocusNode);
                      },
                      onSaved: (newValue) {
                        edittedProduct = Product(
                            id: edittedProduct.id,
                            title: newValue!,
                            description: edittedProduct.description,
                            price: edittedProduct.price,
                            imageUrl: edittedProduct.imageUrl,
                            isFavorite: edittedProduct.isFavorite);
                      },
                      validator: (value) {
                        if (value!.isEmpty) return "Title field is required!";
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      initialValue: formInitialValues['price'],
                      decoration: InputDecoration(labelText: "Price"),
                      focusNode: _priceFocusNode,
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (value) {
                        FocusScope.of(context)
                            .requestFocus(_descriptionFocusNode);
                      },
                      onSaved: (newValue) {
                        edittedProduct = Product(
                            id: edittedProduct.id,
                            title: edittedProduct.title,
                            description: edittedProduct.description,
                            price: double.parse(newValue!),
                            imageUrl: edittedProduct.imageUrl,
                            isFavorite: edittedProduct.isFavorite);
                      },
                      validator: (value) {
                        if (value!.isEmpty) return "Price field is required!";
                        if (double.tryParse(value) == null)
                          return "Please Enter a valid price!";
                        if (double.parse(value) <= 0)
                          return "Price should be greater than 0!";
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      initialValue: formInitialValues['description'],
                      decoration: InputDecoration(labelText: "Description"),
                      focusNode: _descriptionFocusNode,
                      maxLines: 3,
                      keyboardType: TextInputType.multiline,
                      onSaved: (newValue) {
                        edittedProduct = Product(
                            id: edittedProduct.id,
                            title: edittedProduct.title,
                            description: newValue!,
                            price: edittedProduct.price,
                            imageUrl: edittedProduct.imageUrl,
                            isFavorite: edittedProduct.isFavorite);
                      },
                      validator: (value) {
                        if (value!.isEmpty)
                          return "Description field is required!";
                        if (value.length < 10)
                          return "Description must be at least 10 characters!";
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          width: 100,
                          height: 100,
                          child: showImage
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.network(
                                    _imageUrlController.text,
                                    fit: BoxFit.cover,
                                    errorBuilder:
                                        (context, error, stackTrace) => Icon(
                                      Icons.error,
                                      color:
                                          Theme.of(context).colorScheme.error,
                                    ),
                                  ),
                                )
                              : Center(
                                  child: Icon(Icons.image),
                                ),
                          decoration: BoxDecoration(
                              border: Border.all(width: 1, color: Colors.grey),
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        SizedBox(
                          width: 40,
                        ),
                        Expanded(
                          child: TextFormField(
                            decoration: InputDecoration(labelText: "Image Url"),
                            keyboardType: TextInputType.url,
                            controller: _imageUrlController,
                            focusNode: _imageUrlFocusNode,
                            textInputAction: TextInputAction.done,
                            onSaved: (newValue) {
                              edittedProduct = Product(
                                  id: edittedProduct.id,
                                  title: edittedProduct.title,
                                  description: edittedProduct.description,
                                  price: edittedProduct.price,
                                  imageUrl: newValue!,
                                  isFavorite: edittedProduct.isFavorite);
                            },
                            // onFieldSubmitted: (value) {
                            //   onFormSave();
                            // },
                            onChanged: (value) {
                              setState(() {
                                showImage = false;
                              });
                            },
                            validator: (value) {
                              if (value!.isEmpty)
                                return "Image Url field is required!";
                              return null;
                            },
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
