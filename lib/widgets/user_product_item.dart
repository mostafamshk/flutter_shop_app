// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/screens/edit_user_product_screen.dart';

import '../providers/products.dart';

class UserProductItem extends StatelessWidget {
  final String productId;
  final String title;
  final String imageUrl;

  const UserProductItem({
    Key? key,
    required this.productId,
    required this.title,
    required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final scaffoldMessenger = ScaffoldMessenger.of(context);
    return ListTile(
      contentPadding: EdgeInsets.all(10),
      leading: CircleAvatar(
          backgroundImage: NetworkImage(
        imageUrl,
      )),
      title: Text(title),
      trailing: SizedBox(
        width: 100,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(
                      EditUserProductScreen.routeName,
                      arguments: productId);
                },
                icon: Icon(Icons.edit,
                    color: Theme.of(context).colorScheme.primary)),
            IconButton(
                onPressed: () async {
                  try {
                    await Provider.of<Products>(context, listen: false)
                        .deleteProduct(productId);
                  } catch (error) {
                    scaffoldMessenger.showSnackBar(
                        SnackBar(content: Text(error.toString())));
                  }
                },
                icon: Icon(Icons.delete,
                    color: Theme.of(context).colorScheme.error))
          ],
        ),
      ),
    );
  }
}
