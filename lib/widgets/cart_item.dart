// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/cart.dart';
import '../providers/cart_item.dart';

class CartItemWidget extends StatelessWidget {
  final CartItem cartItem;

  const CartItemWidget({
    Key? key,
    required this.cartItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context, listen: false);
    return Column(
      children: [
        Dismissible(
          confirmDismiss: (direction) => showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
              actions: [
                TextButton(
                    onPressed: () => Navigator.of(ctx).pop(true),
                    child: Text("Yes")),
                TextButton(
                    onPressed: () => Navigator.of(ctx).pop(false),
                    child: Text("No")),
              ],
              title: Text("Delete from order?"),
              content: Text(
                  "Are you sure about deleting this product from your cart?"),
            ),
          ),
          onDismissed: (direction) {
            cart.removeWholeProductFromCart(cartItem: cartItem);
          },
          direction: DismissDirection.endToStart,
          background: Container(
            color: Theme.of(context).colorScheme.error,
            alignment: Alignment.centerRight,
            padding: EdgeInsets.only(right: 10),
            child: Icon(
              Icons.delete,
              color: Colors.white,
              size: 30,
            ),
          ),
          key: ValueKey(cartItem.id),
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Theme.of(context).colorScheme.primary,
              foregroundColor: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: FittedBox(
                    child: Text("${cartItem.price.toStringAsFixed(2)}")),
              ),
            ),
            title: Text(cartItem.title),
            trailing: Text("${cartItem.quantity} x"),
          ),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }
}
