// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../providers/orders.dart';

class OrderItemWidget extends StatefulWidget {
  const OrderItemWidget({
    Key? key,
    required this.orderId,
  }) : super(key: key);

  final String orderId;

  @override
  State<OrderItemWidget> createState() => _OrderItemWidgetState();
}

class _OrderItemWidgetState extends State<OrderItemWidget> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    final order = Provider.of<Orders>(context)
        .orders
        .firstWhere((order) => order.id == widget.orderId);
    return Card(
      elevation: 10,
      child: Container(
        child: Column(
          children: [
            ListTile(
              title: Text("\$ ${order.amount.toStringAsFixed(2)}"),
              subtitle:
                  Text(DateFormat("MMM dd yyyy, hh:mm").format(order.dateTime)),
              trailing: IconButton(
                  onPressed: () {
                    setState(() {
                      isExpanded = !isExpanded;
                    });
                  },
                  icon: Icon(Icons.expand_more)),
            ),
            // if (isExpanded)
            AnimatedContainer(
              duration: const Duration(milliseconds: 300),
              curve: Curves.easeIn,
              width: double.infinity,
              height: isExpanded ? min(order.products.length * 60, 240) : 0,
              child: ListView.builder(
                itemBuilder: (context, index) => ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Theme.of(context).colorScheme.primary,
                    foregroundColor: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: FittedBox(
                          child: Text(
                              "${order.products[index].price.toStringAsFixed(2)}")),
                    ),
                  ),
                  title: Text(order.products[index].title),
                  trailing: Text("${order.products[index].quantity} x"),
                ),
                itemCount: order.products.length,
              ),
            )
          ],
        ),
      ),
    );
  }
}
