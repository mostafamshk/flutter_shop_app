// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:shopapp/screens/product-detail.dart';
import 'package:shopapp/widgets/product_item.dart';

import '../providers/product.dart';
import '../providers/products.dart';

class ProductsGrid extends StatelessWidget {
  final bool showFavorites;

  const ProductsGrid({
    Key? key,
    required this.showFavorites,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Products productsProvider = Provider.of<Products>(context);
    List<Product> products =
        showFavorites ? productsProvider.favoriteItems : productsProvider.items;
    bool isLandscape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: isLandscape ? 3 : 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20),
        itemBuilder: (ctx, index) {
          return ChangeNotifierProvider.value(
              value: products[index], child: ProductItem());
        },
        itemCount: showFavorites
            ? productsProvider.favoriteItems.length
            : productsProvider.items.length);
  }
}
