// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/auth.dart';
import '../providers/cart.dart';
import '../providers/product.dart';
import '../providers/products.dart';
import '../screens/product-detail.dart';

class ProductItem extends StatelessWidget {
  const ProductItem({super.key});

  @override
  Widget build(BuildContext context) {
    final scaffoldMessenger = ScaffoldMessenger.of(context);
    final product = Provider.of<Product>(context, listen: false);
    final products = Provider.of<Products>(context, listen: false);
    final cart = Provider.of<Cart>(context, listen: false);
    final auth = Provider.of<Auth>(context, listen: false);

    return GestureDetector(
      onTap: () {
        Navigator.of(context)
            .pushNamed(ProductDetailScreen.routeName, arguments: product.id);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: GridTile(
          child: Hero(
            tag: product.id,
            child: FadeInImage(
              fit: BoxFit.cover,
              image: NetworkImage(
                product.imageUrl,
              ),
              placeholder: AssetImage('assets/images/product_placeholder.png'),
            ),
          ),
          footer: GridTileBar(
              backgroundColor: Colors.black87,
              leading: IconButton(
                  onPressed: () async {
                    try {
                      await product.toggleIsFavorite(
                          products.authToken, auth.userId);
                    } catch (error) {
                      scaffoldMessenger.showSnackBar(
                          SnackBar(content: Text(error.toString())));
                    }
                  },
                  icon: Consumer<Product>(
                    builder: (ctx, productx, lastWidget) => Icon(
                      productx.isFavorite
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                  )),
              title: Text(product.title),
              trailing: IconButton(
                  onPressed: () {
                    cart.addToCart(product.id, product.price, product.title);
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      duration: Duration(seconds: 2),
                      content: Text("Product added to cart!"),
                      action: SnackBarAction(
                        label: "Undo!",
                        onPressed: () {
                          cart.removeOneProductFromCart(product.id);
                        },
                      ),
                    ));
                  },
                  icon: Icon(
                    Icons.shopping_cart,
                    color: Theme.of(context).colorScheme.secondary,
                  ))),
        ),
      ),
    );
  }
}
