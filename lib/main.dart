import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/helpers/custom_routing_transition.dart';
import 'package:shopapp/providers/auth.dart';
import 'package:shopapp/screens/auth_screen.dart';
import 'package:shopapp/screens/edit_user_product_screen.dart';
import 'package:shopapp/screens/splash_screen.dart';
import 'package:shopapp/screens/user_products_screen.dart';

import './providers/cart.dart';
import './providers/orders.dart';
import './providers/products.dart';

import './screens/cart_screen.dart';
import './screens/orders_screen.dart';
import './screens/product-detail.dart';
import './screens/products_overview_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Auth auth = Auth();

  MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: auth),
          ChangeNotifierProxyProvider<Auth, Products>(
            create: (ctx) => Products([], null, null),
            update: (context, authData, previousProducts) => Products(
                previousProducts == null ? [] : previousProducts.items,
                authData.token,
                authData.userId),
          ),
          ChangeNotifierProxyProvider<Auth, Cart>(
            create: (ctx) => Cart({}, null),
            update: (context, authData, previousCart) => Cart(
                previousCart == null ? {} : previousCart.items, authData.token),
          ),
          ChangeNotifierProxyProvider<Auth, Orders>(
            create: (ctx) => Orders([], null, null),
            update: (context, authData, previousOrders) => Orders(
                previousOrders == null ? [] : previousOrders.orders,
                authData.token,
                authData.userId),
          ),
        ],
        child: Consumer<Auth>(
          builder: (ctx, authData, child) => MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            theme: ThemeData(
              fontFamily: 'Lato',
              colorScheme: ColorScheme.fromSwatch(
                  primarySwatch: Colors.purple, accentColor: Colors.deepOrange),
              pageTransitionsTheme: PageTransitionsTheme(builders: {
                TargetPlatform.android: CustomPageTransitionBuilder(),
                TargetPlatform.iOS: CustomPageTransitionBuilder()
              }),
            ),
            home: authData.isAuth
                ? ProductsOverviewScreen()
                : FutureBuilder(
                    future: auth.tryToLogin(),
                    builder: (context, snapshot) =>
                        snapshot.connectionState == ConnectionState.waiting
                            ? SplashScreen()
                            : AuthScreen()),
            routes: {
              AuthScreen.routeName: (ctx) => AuthScreen(),
              ProductsOverviewScreen.routeName: (ctx) =>
                  ProductsOverviewScreen(),
              ProductDetailScreen.routeName: (ctx) => ProductDetailScreen(),
              OrdersScreen.routeName: (ctx) => OrdersScreen(),
              CartScreen.routeName: (ctx) => CartScreen(),
              UserProductsScreen.routeName: (ctx) => UserProductsScreen(),
              EditUserProductScreen.routeName: (ctx) => EditUserProductScreen()
            },
          ),
        ));
  }
}
