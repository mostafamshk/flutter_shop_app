// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:shopapp/models/http_exception.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite = false;
  Product(
      {required this.id,
      required this.title,
      required this.description,
      required this.price,
      required this.imageUrl,
      this.isFavorite = false});

  Future toggleIsFavorite(String? token, String? userId) async {
    final String url =
        'https://shopapp-4d9e5-default-rtdb.firebaseio.com/usersFavorites/${userId}/${id}.json?auth=$token';
    isFavorite = !isFavorite;
    notifyListeners();
    try {
      final response =
          await http.put(Uri.parse(url), body: json.encode(isFavorite));
      if (response.statusCode != 200) {
        throw HttpException("Could not toggle!");
      }
    } catch (error) {
      isFavorite = !isFavorite;
      notifyListeners();
      rethrow;
    }
  }
}
