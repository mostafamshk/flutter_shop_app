// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import 'package:shopapp/models/http_exception.dart';

import 'product.dart';

class Products with ChangeNotifier {
  List<Product> _items = [];
  String? _authToken;
  String? _userId;
  Products(this._items, this._authToken, this._userId);

  String? get authToken {
    return "${_authToken}";
  }

  List<Product> get items {
    return [..._items];
  }

  List<Product> get favoriteItems {
    return _items.where((element) => element.isFavorite).toList();
  }

  Future<void> addProduct(Product product) async {
    final url =
        'https://shopapp-4d9e5-default-rtdb.firebaseio.com/products.json?auth=$_authToken';

    try {
      final response = await http.post(
          Uri.parse(
            url,
          ),
          body: json.encode({
            'title': product.title,
            'price': product.price,
            'description': product.description,
            'isFavorite': product.isFavorite,
            'imageUrl': product.imageUrl,
            'creatorId': _userId
          }));
      final productId = json.decode(response.body)['name'];
      _items.insert(
          0,
          Product(
              id: productId,
              title: product.title,
              description: product.description,
              price: product.price,
              imageUrl: product.imageUrl));
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future getProducts(bool filterByCreatorId) async {
    final String filterQuery =
        filterByCreatorId ? '&orderBy="creatorId"&equalTo="${_userId}"' : '';
    final url =
        'https://shopapp-4d9e5-default-rtdb.firebaseio.com/products.json?auth=$_authToken$filterQuery';
    final userFavoritesUrl =
        'https://shopapp-4d9e5-default-rtdb.firebaseio.com/usersFavorites/${_userId}.json?auth=$_authToken';
    try {
      final response = await http.get(Uri.parse(url));
      final productsMap = json.decode(response.body) as Map<String, dynamic>;
      final userFavoritesResponse = await http.get(Uri.parse(userFavoritesUrl));
      final userFavoritesMap = json.decode(userFavoritesResponse.body);

      List<Product> loadedProducts = [];
      productsMap.forEach((key, value) {
        loadedProducts.add(Product(
            id: key,
            title: value['title'],
            description: value['description'],
            price: value['price'],
            imageUrl: value['imageUrl'],
            isFavorite: userFavoritesMap == null
                ? false
                : userFavoritesMap[key] ?? false));
      });
      _items = loadedProducts;
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future updateProduct(String id, Product product) async {
    final productIndex = _items.indexWhere((element) => element.id == id);
    final String url =
        'https://shopapp-4d9e5-default-rtdb.firebaseio.com/products/$id.json?auth=$_authToken';

    try {
      await http.patch(Uri.parse(url),
          body: json.encode({
            'title': product.title,
            'description': product.description,
            'imageUrl': product.imageUrl,
            'price': product.price
          }));
      _items[productIndex] = Product(
          id: product.id,
          title: product.title,
          description: product.description,
          price: product.price,
          imageUrl: product.imageUrl,
          isFavorite: product.isFavorite);

      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future deleteProduct(String id) async {
    final String url =
        'https://shopapp-4d9e5-default-rtdb.firebaseio.com/products/$id.json?auth=$_authToken';
    final deletingProductInex =
        _items.indexWhere((element) => element.id == id);
    final deletingProduct = _items.elementAt(deletingProductInex);
    _items.removeWhere((element) => element.id == id);
    notifyListeners();
    try {
      final response = await http.delete(Uri.parse(url));
      if (response.statusCode >= 400) {
        throw HttpException("Could not delete!");
      }
    } catch (error) {
      _items.insert(deletingProductInex, deletingProduct);
      notifyListeners();
      rethrow;
    }
  }

  bool hasFavorites() {
    return _items.any((element) => element.isFavorite);
  }

  Product findById(String id) {
    return _items.firstWhere((element) => element.id == id);
  }
}
