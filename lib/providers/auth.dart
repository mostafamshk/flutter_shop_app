import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Auth with ChangeNotifier {
  String? _token;
  DateTime? _expiryDate;
  String? _userId;

  Timer? _expiryTimer;

  String? get userId {
    return _userId;
  }

  String? get token {
    return _token != null &&
            _expiryDate != null &&
            _expiryDate!.isAfter(DateTime.now())
        ? _token
        : null;
  }

  bool get isAuth {
    return token != null;
  }

  Future<void> _authenticate(String email, String password, String url) async {
    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode({
            'email': email,
            'password': password,
            'returnSecureToken': true
          }));

      final responseData = json.decode(response.body);
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      _token = responseData['idToken'];
      _expiryDate = DateTime.now()
          .add(Duration(seconds: int.parse(responseData['expiresIn'])));
      _userId = responseData['localId'];
      _logoutTimer();
      notifyListeners();

      final prefs = await SharedPreferences.getInstance();
      prefs.setString(
          "userData",
          json.encode({
            'token': _token,
            'userId': _userId,
            'expiryDate': _expiryDate.toString()
          }));
    } catch (exception) {
      rethrow;
    }
  }

  Future<void> signup(String email, String password) async {
    return _authenticate(email, password,
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAzNkAoFvtGEAHeqGqXKJ-mk5yRAs1HZhw");
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password,
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAzNkAoFvtGEAHeqGqXKJ-mk5yRAs1HZhw");
  }

  void logout() async {
    _token = null;
    _userId = null;
    _expiryDate = null;
    if (_expiryTimer != null) {
      _expiryTimer!.cancel();
      _expiryTimer = null;
    }
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.remove("userData");
  }

  void _logoutTimer() {
    if (_expiryTimer != null) {
      _expiryTimer!.cancel();
    }
    int seconds = _expiryDate!.difference(DateTime.now()).inSeconds;
    _expiryTimer = Timer(Duration(seconds: seconds), () {
      logout();
    });
  }

  Future<bool> tryToLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey("userData")) {
      return false;
    }
    final authData = json.decode(prefs.getString("userData")!);
    DateTime extractedExpiryDate = DateTime.parse(authData['expiryDate']);
    if (extractedExpiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = authData['token'];
    _userId = authData['userId'];
    _expiryDate = extractedExpiryDate;
    notifyListeners();
    _logoutTimer();
    return true;
  }
}
