// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/foundation.dart';

import 'package:shopapp/providers/cart_item.dart';

class Cart with ChangeNotifier {
  Map<String, CartItem> _items;
  final String? _authToken;
  Cart(
    this._items,
    this._authToken,
  );

  Map<String, CartItem> get items {
    return {..._items};
  }

  void addToCart(String productId, double price, String title) {
    if (_items.containsKey(productId)) {
      _items.update(
          productId,
          (lastValue) => CartItem(
              id: lastValue.id,
              title: lastValue.title,
              quantity: lastValue.quantity + 1,
              price: lastValue.price));
    } else {
      _items.putIfAbsent(
          productId,
          () => CartItem(
              id: DateTime.now().toString(),
              title: title,
              quantity: 1,
              price: price));
    }
    notifyListeners();
  }

  void removeWholeProductFromCart({String? productId, CartItem? cartItem}) {
    productId == null
        ? _items.removeWhere((key, value) => value == cartItem)
        : _items.remove(productId);
    notifyListeners();
  }

  void removeOneProductFromCart(String productId) {
    if (_items[productId]!.quantity > 1) {
      _items.update(
          productId,
          (lastValue) => CartItem(
              id: lastValue.id,
              title: lastValue.title,
              quantity: lastValue.quantity - 1,
              price: lastValue.price));
    } else {
      _items.remove(productId);
    }
    notifyListeners();
  }

  int get cartItemsCount {
    return _items.length;
  }

  int get allProductsCount {
    int sum = 0;
    _items.forEach((key, value) {
      sum += value.quantity;
    });
    return sum;
  }

  bool isProductInCart(String productId) {
    return _items.containsKey(productId);
  }

  double totalExpense() {
    double sum = 0;
    _items.forEach((key, value) {
      sum += (value.quantity * value.price);
    });

    return sum;
  }

  void clear() {
    _items = {};
    notifyListeners();
  }
}
