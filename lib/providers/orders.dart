// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'cart_item.dart';

class OrderItem {
  final String id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  OrderItem(
      {required this.id,
      required this.amount,
      required this.products,
      required this.dateTime});
}

class Orders with ChangeNotifier {
  List<OrderItem> _orders;
  final String? _authToken;
  final String? _userId;
  Orders(this._orders, this._authToken, this._userId);

  List<OrderItem> get orders {
    return [..._orders];
  }

  Future addOrder(List<CartItem> cartProducts, double total) async {
    final url =
        'https://shopapp-4d9e5-default-rtdb.firebaseio.com/orders/${_userId}.json?auth=$_authToken';
    try {
      List products = [];
      products = cartProducts.map((element) {
        return {
          'id': element.id,
          'title': element.title,
          'quantity': element.quantity,
          'price': element.price
        };
      }).toList();
      final timeStamp = DateTime.now().toString();
      Map<String, dynamic> requestBody = {
        'amount': total,
        'dateTime': timeStamp,
        'products': products
      };
      final response =
          await http.post(Uri.parse(url), body: json.encode(requestBody));
      if (response.statusCode != 200) {
        throw HttpException("Could not add the order!");
      }
      String id = json.decode(response.body)['name'];
      _orders.insert(
          0,
          OrderItem(
              id: id,
              amount: total,
              dateTime: DateTime.parse(timeStamp),
              products: cartProducts));
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future getOrders() async {
    final url =
        'https://shopapp-4d9e5-default-rtdb.firebaseio.com/orders/${_userId}.json?auth=$_authToken';
    try {
      final response = await http.get(Uri.parse(url));
      if (response.statusCode != 200) {
        throw HttpException("Could not load your orders!");
      }
      final loadedOrdersMap =
          json.decode(response.body) as Map<String, dynamic>;
      List<OrderItem> loadedOrders = [];
      loadedOrdersMap.forEach((key, value) {
        List<CartItem> cartItems = [];
        cartItems = (value['products'] as List)
            .map((e) => CartItem(
                id: e['id'],
                title: e['title'],
                quantity: e['quantity'],
                price: e['price']))
            .toList();
        loadedOrders.add(OrderItem(
            id: key,
            amount: value['amount'],
            products: cartItems,
            dateTime: DateTime.parse(value['dateTime'])));
      });
      _orders = loadedOrders;
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }
}
